﻿using System;

namespace FirstChannelTest.RegExSearch.Exceptions
{
    internal class RegExSearcherConfigException : Exception
    {
        public RegExSearcherConfigException()
        {
        }

        public RegExSearcherConfigException(string message)
            : base(message)
        {
        }

        public RegExSearcherConfigException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}