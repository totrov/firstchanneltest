﻿using System.IO;

namespace FirstChannelTest.RegExSearch
{
    /// <summary>
    /// Exposes class that relalizes RegExSearch from concrete type source
    /// </summary>
    /// <remarks>
    /// Implementations should be registered in app.config (RegExSearchers section)
    /// </remarks>
    public interface IRegExSearcher
    {
        void SetFileInfo(FileInfo fileInfo);
        bool CheckRegEx(string regEx);
    }
}