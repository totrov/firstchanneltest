﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using FirstChannelTest.Infrastructure.Log;
using FirstChannelTest.RegExSearch.ConcreteSerchers;
using FirstChannelTest.RegExSearch.Exceptions;

namespace FirstChannelTest.RegExSearch
{
    public static class SercherResolver
    {
        private static Dictionary<string, Type> _searchersDictionary;

        public static IRegExSearcher ResolveByExtension(string extention)
        {
            var section = (RegExSearchConfigurationSectionHandler)ConfigurationManager.GetSection("RegExSearchers");
            var searcherTypeName = GetResolverTypeNameFromConfig(extention, section);
            if (string.IsNullOrEmpty(searcherTypeName))
            {
                Logger.Log.WarnFormat(extention + " is not configured in SearchResolvers section of App.config file. Default (txt) sercher is set.");
                return new DefaultTextSearcher();
            }
            IRegExSearcher searcher;
            try
            {
                searcher = GetSearcherInstanceByTypeName(searcherTypeName);
            }
            catch (RegExSearcherConfigException)
            {
                if (OfferDefaultSearch())
                    return new DefaultTextSearcher();
                else throw;
            }
            return searcher;
        }

        private static bool OfferDefaultSearch()
        {
            while (true)
            {
                Console.Write("Configured search has failed. Do you want to continue with default search? Type yes to continue, no to exit.");
                var answer = Console.ReadLine();
                if (answer == "yes")
                    return true;
                if (answer == "no")
                    return false;
            }
        }

        private static IRegExSearcher GetSearcherInstanceByTypeName(string searcherTypeName)
        {
            Type sercherType;
            try
            {
                sercherType = Assembly.GetExecutingAssembly().GetType(searcherTypeName);
            }
            catch (Exception e)
            {
                Logger.Log.ErrorFormat("getting type {0} caused an {1} exception with message:\"{2}\"",
                    searcherTypeName,
                    e.GetType().Name,
                    e.Message);
                throw new RegExSearcherConfigException("cannot find searcher type by name", e);
            }
            if(sercherType == null)
                throw new RegExSearcherConfigException("Type of searcher does not exist in assembly");
            return CreateInstanceOfSearcher(sercherType);
        }

        private static IRegExSearcher CreateInstanceOfSearcher(Type sercherType)
        {
            IRegExSearcher searcher;
            if (!typeof(IRegExSearcher).IsAssignableFrom(sercherType))
            {
                throw new RegExSearcherConfigException(sercherType + ", set in SearchResolvers section of App.config not implements ISearcher interface");
            }
            try
            {
                searcher = (IRegExSearcher)Activator.CreateInstance(sercherType);
            }
            catch (Exception e)
            {
                var message = string.Format("Cannot create an instance of {0}. See inner exception", sercherType);
                throw new RegExSearcherConfigException(message, e);
            }
            return searcher;
        }

        private static string GetResolverTypeNameFromConfig(string extention, RegExSearchConfigurationSectionHandler section)
        {
            foreach (SearcherElement searcherItem in section.SearcherItems)
            {
                if (!searcherItem.FileExtension.ToLower().Equals(extention)) continue;
                Logger.Log.Info(extention + " is configured to be processed by " + searcherItem.SearcherType);
                return searcherItem.SearcherType;
            }
            return null;
        }
    }
}
