﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using OfficeOpenXml;

namespace FirstChannelTest.RegExSearch.ConcreteSerchers
{
    public class ExcelSearcher : IRegExSearcher
    {
        private FileInfo _fileInfo;
        public void SetFileInfo(FileInfo fileInfo)
        {
            _fileInfo = fileInfo;
        }

        public bool CheckRegEx(string regEx)
        {
            Regex regex;
            try
            {
                regex = new Regex(regEx);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("RegEx has error. Please, check RegEx and try again. Press any key to exit.");
                Console.ReadKey(true);
                throw;
            }
            using (ExcelPackage package = new ExcelPackage(_fileInfo))
            {
                foreach (var sheet in package.Workbook.Worksheets)
                {
                    var start = sheet.Dimension.Start;
                    var end = sheet.Dimension.End;
                    for (int row = start.Row; row <= end.Row; row++)
                    {
                        for (int col = start.Column; col <= end.Column; col++)
                        {
                            string value = sheet.Cells[row, col].Text ?? string.Empty;
                            try
                            {
                                if (regex.IsMatch(value))
                                    return true;
                            }
                            catch (ArgumentException)
                            {
                                Console.WriteLine("RegEx has error. Please, check RegEx and try again. Press any key to exit.");
                                Console.ReadKey(true);
                                throw;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
}
