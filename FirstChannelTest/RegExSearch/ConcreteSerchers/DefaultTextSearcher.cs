﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using FirstChannelTest.Infrastructure.Log;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;

namespace FirstChannelTest.RegExSearch.ConcreteSerchers
{
    public class DefaultTextSearcher : IRegExSearcher
    {
        private FileInfo _fileInfo;
        public void SetFileInfo(FileInfo fileInfo)
        {
            _fileInfo = fileInfo;
        }

        public bool CheckRegEx(string regEx)
        {
            string text;
            using (var streamReader = new StreamReader(_fileInfo.FullName))
            {
                try
                {
                    text = streamReader.ReadToEnd();
                    //на практике плохо, если файл может быть большим, но для экономии времени в тестовом примере напишу так
                }
                catch (OutOfMemoryException)
                {
                    Console.WriteLine("File is too large. Please, reduse size of file and try again. Press any key to exit.");
                    Console.ReadKey(true);
                    throw;
                }
            }
            bool isMatch;
            try
            {
                isMatch = Regex.IsMatch(text, regEx);
            }
            catch (RegexMatchTimeoutException)
            {
                Console.WriteLine("File is too large. Please, reduse size of file and try again. Press any key to exit.");
                Console.ReadKey(true);
                throw;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("File or regEx has error. Please, check RegEx and try again. Press any key to exit.");
                Console.ReadKey(true);
                throw;
            }
            return isMatch;
        }
    }
}
