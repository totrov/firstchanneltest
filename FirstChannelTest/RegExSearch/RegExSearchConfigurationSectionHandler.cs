﻿using System.Configuration;

namespace FirstChannelTest.RegExSearch
{
    public class RegExSearchConfigurationSectionHandler : System.Configuration.ConfigurationSection
    {
        [ConfigurationProperty("Searchers")]
        public ResolversCollection SearcherItems
        {
            get { return ((ResolversCollection)(base["Searchers"])); }
        }
    }

    [ConfigurationCollection(typeof(SearcherElement))]
    public class ResolversCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new SearcherElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SearcherElement)(element)).FileExtension;
        }

        public SearcherElement this[int idx]
        {
            get { return (SearcherElement)BaseGet(idx); }
        }
    }

    public class SearcherElement : ConfigurationElement
    {

        [ConfigurationProperty("fileExtesion", IsKey = true, IsRequired = true)]
        public string FileExtension
        {
            get { return ((string)(base["fileExtesion"])); }
            set { base["fileExtesion"] = value; }
        }

        [ConfigurationProperty("sercherType", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string SearcherType
        {
            get { return ((string)(base["sercherType"])); }
            set { base["sercherType"] = value; }
        }
    }
}
