﻿using System;
using System.IO;
using System.Security;
using System.Text;
using FirstChannelTest.Infrastructure.Log;
using FirstChannelTest.RegExSearch;

namespace FirstChannelTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger.InitLogger();
            Logger.Log.Info("Application Started");

            Console.Write("Enter path:");
            var pathString = Console.ReadLine();
            Console.Write("Enter regEx:");
            var regEx = Console.ReadLine();

            Environment.ExitCode = RegexpSearchInFile(pathString, regEx);
        }

        private static int RegexpSearchInFile(string pathString, string regEx)
        {
            Logger.Log.Info("Start of search operation...");
            try
            {
                FileInfo fileInfo = GetFileInfoByPath(pathString);
                var searcher = SercherResolver.ResolveByExtension(fileInfo.Extension);
                searcher.SetFileInfo(fileInfo);
                if (searcher.CheckRegEx(regEx))
                {
                    Logger.Log.Info("Search operation finished successfully. RegEx was found");
                    return 1;
                }
                Logger.Log.Info("Search operation finished successfully. RegEx was not found");
                return 0;
            }
            catch (Exception e)
            {
                Logger.Log.Error("Operation stopped with unhandled exception", e);
            }
            return 0;
        }

        private static FileInfo GetFileInfoByPath(string pathString)
        {
            FileInfo fileInfo;
            try
            {
                Logger.Log.Info("Trying to find file by path...");
                fileInfo = new FileInfo(pathString);
                if (!fileInfo.Exists)
                {
                    Logger.Log.Error("File not exists! End of search operation.");
                    throw new FileNotFoundException();
                }
            }
            catch (ArgumentNullException)
            {
                Logger.Log.Error("Path is null");
                throw;
            }
            catch (SecurityException)
            {
                Logger.Log.Error("The caller does not have the required permission.");
                throw;
            }
            catch (ArgumentException)
            {
                Logger.Log.Error("The file name is empty, contains only white spaces, or contains invalid characters.");
                throw;
            }
            catch (UnauthorizedAccessException)
            {
                Logger.Log.Error("The file name is empty, contains only white spaces, or contains invalid characters.");
                throw;
            }
            catch (PathTooLongException)
            {
                Logger.Log.Error("The specified path, file name, or both exceed the system-defined maximum length.");
                throw;
            }
            catch (NotSupportedException)
            {
                Logger.Log.Error("File name contains a colon (:) in the middle of the string.");
                throw;
            }
            Logger.Log.Info("File found.");
            return fileInfo;
        }
    }
}
